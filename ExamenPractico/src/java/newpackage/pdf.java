/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package newpackage;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Calendar;
import javax.imageio.ImageIO;


@WebServlet(name = "pdf", urlPatterns = {"/pdf"})
public class pdf extends HttpServlet {

    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/pdf");
        OutputStream out = response.getOutputStream();
        String nombre= request.getParameter("Text_name");
        try{
            try{
                
                Document documento = new Document();
                PdfWriter.getInstance(documento, out);
                Calendar fecha = Calendar.getInstance();
                //******** AQUI COMENZAMOS A ESCRIBIR EN EL PDF *************************//
                documento.open();
                Paragraph par1= new Paragraph();
                Font fonttitulo= new Font(Font.FontFamily.TIMES_ROMAN,30,Font.BOLD, BaseColor.DARK_GRAY);
                Font fontdocumento= new Font(Font.FontFamily.TIMES_ROMAN,18,Font.NORMAL, BaseColor.BLACK);
                Rectangle rect= new Rectangle(10, 10,585,830);
                rect.setBorder(Rectangle.BOX);
                rect.setBorderWidth(8);
                rect.setBorderColor(BaseColor.YELLOW);
                documento.add(rect);
                Rectangle rect2= new Rectangle(15, 15,580,825);
                rect2.setBorder(Rectangle.BOX);
                rect2.setBorderWidth(4);
                rect2.setBorderColor(BaseColor.RED);
                documento.add(rect2);
                par1.setAlignment(Element.ALIGN_CENTER);
//                Image imagen1 = Image.getInstance("/home/erikgoh/NetBeansProjects/parcial2_certificacion/ExamenPractico/imagenes_certificados/medalla.jpeg");
//                imagen1.setAlignment(Element.ALIGN_CENTER);
//                imagen1.scaleToFit(200, 200);
//                par1.add(imagen1);
                par1.add(new Phrase(Chunk.NEWLINE));
                par1.add(new Phrase(Chunk.NEWLINE));
                par1.add(new Phrase("CERTIFICADO COMPLETADO", fonttitulo));
                par1.add(new Phrase(Chunk.NEWLINE));
                par1.add(new Phrase(Chunk.NEWLINE));
                par1.add(new Phrase(Chunk.NEWLINE));
                par1.add(new Phrase(Chunk.NEWLINE));
                par1.add(new Phrase("CERTIFICADO DE ", fontdocumento));       //NOMBRE DE LA CERTIFICACION//
                par1.add(new Phrase(Chunk.NEWLINE));
                par1.add(new Phrase(Chunk.NEWLINE));
                par1.add(new Phrase(Chunk.NEWLINE));
                par1.add(new Phrase("A NOMBRE DE: "+nombre, fontdocumento));       //NOMBRE DEL USUARIO//
                par1.add(new Phrase(Chunk.NEWLINE));
                par1.add(new Phrase(Chunk.NEWLINE));
                par1.add(new Phrase(Chunk.NEWLINE));
                par1.add(new Phrase("CALIFICACIÓN: ", fontdocumento));      //CALIFICACION DEÑ USUARIO//
                par1.add(new Phrase(Chunk.NEWLINE));
                par1.add(new Phrase(Chunk.NEWLINE));
                par1.add(new Phrase(Chunk.NEWLINE));
                par1.add(new Phrase("FECHA DE EXPEDICION: "+fecha.get(Calendar.DATE)+"/"+fecha.get(Calendar.MONTH)+"/"+fecha.get(Calendar.YEAR),fontdocumento));

                
                documento.add(par1);
                
                documento.close();
                //******** AQUI TERMINAMOS DE ESCRIBIR EN EL PDF *************************//
            }catch(Exception ex){ex.getMessage();}
        } finally{
         out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
