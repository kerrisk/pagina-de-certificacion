/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package misclases;

/**
 *
 * @author erikgoh
 */
public class Cuentas {

    private String cuenta, password;

    public Cuentas(String cuenta, String password) {
        this.cuenta = cuenta;
        this.password = password;
    }

    public Cuentas() {
    }

    public String getCuenta() {
        return cuenta;
    }

    public void setCuenta(String cuenta) {
        this.cuenta = cuenta;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
