/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package misclases;

/**
 *
 * @author erikgoh
 */
public class Respuesta {
    private String Respuesta;
    private Boolean correcta;
    private int valor;

    public Respuesta() {
    }

    public Respuesta(String Respuesta, Boolean correcta, int valor) {
        this.Respuesta = Respuesta;
        this.correcta = correcta;
        this.valor = valor;
    }

    public int getValor() {
        return valor;
    }

    public void setValor(int valor) {
        this.valor = valor;
    }

    
    public String getRespuesta() {
        return Respuesta;
    }

    public void setRespuesta(String Respuesta) {
        this.Respuesta = Respuesta;
    }

    public Boolean getCorrecta() {
        return correcta;
    }

    public void setCorrecta(Boolean correcta) {
        this.correcta = correcta;
    }
    
}
