<%-- 
    Document   : Cert_FinPer
    Created on : 29/10/2018, 01:10:10 AM
    Author     : erikgoh
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Certificacion Financiera</title>
        <link rel="stylesheet" href="css/style.css">
        <meta name="viewport" content="width=device=width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"  crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
        <link id="favicon" rel="icon" type="image/png" href="images/favicon.ico">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" crossorigin="anonymous">
        <link href="https://fonts.googleapis.com/css?family=Noto+Sans+KR" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
        <link rel="stylesheet" href="css/animate.css">
    </head>
    <body>
        <%@include file="header.jsp"%>
        <article>
            <h3>Certificacion Finanzas Personales</h3>
            <br>
            <hr>
            <br>
            <h1>Acerca de este curso</h1>
            <p>Este certificado evaluará que sus participantes con las herramientas otorgadas, sea capaz de llevar a cabo una planeación financiera personal y entender la necesidad de salvaguardar su patrimonio y recursos financieros, así como los requerimientos mínimos que se deben considerar para tomar buenas decisiones financieras.
            </p>
            <br>
            <h1>Objetivos</h1>
            <ul>
                <li>Saber planear los gastos familiares por medio de un presupuesto es muy importante, sobre todo cuando la situación económica no es la ideal.</li>
                <li>Comprender que el ahorrar es destinar una parte de los ingresos para conseguir una meta o crear un fondo que permita enfrentar una emergencia.</li>
                <li>Que comprenda que invertir es destinar una parte de los ingresos a actividades productivas con el propósito de obtener un rendimiento o incremento del patrimonio.</li>
            </ul>
            <%
                if (!loggedin) { %>
            <h4>Debes Registrarte para poder hacer el examen de certificacion.</h4>
            <button data-dismiss="modal" type="button"  class="btn btn-login btn-sm" data-toggle="modal" data-target="#registro" >Registro</button>  
            <%
            } else { %>
            <h4>El examen de certificacion no esta disponible en estos momentos.</h4> 
            <button type="button"  class="btn btn-primary btn-sm disabled"><a href="">Iniciar Test</a></button>  
            <%
                }
            %>
        </article>
        <jsp:include page="footer.html"/>
    </body>
</html>