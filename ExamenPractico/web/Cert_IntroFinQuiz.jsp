<%-- 
    Document   : Cert_IntroFinQuiz
    Created on : 29/10/2018, 02:20:22 AM
    Author     : erikgoh
--%>

<%@page import="java.time.LocalDateTime"%>
<%@page import="java.time.format.DateTimeFormatter"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.Collections"%>
<%@page import="java.util.Collection"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.io.File" %>
<%@page import="java.io.FileNotFoundException" %>
<%@page import="java.util.Scanner" %>
<%@page session="true" %>
<%@page import="misclases.Pregunta" %>
<%@page import="misclases.Respuesta" %>
<%@page import="java.util.concurrent.ThreadLocalRandom;" %>
<%
    
    String Archivo = application.getRealPath("/") + "Examen_Finanzas";
    File file = new File(Archivo);
    Scanner sc = new Scanner(file);
    ArrayList<Pregunta> array = new ArrayList();
    // we just need to use \\n as delimiter 
    sc.useDelimiter("\n");

    while (sc.hasNext()) {

//         out.println(sc.next()+"<br>");
        Pregunta sp = new Pregunta();
        ArrayList<Respuesta> respuestas = sp.getRespuestas();
        sp.setPregunta(sc.next());
        respuestas.add(new Respuesta(sc.next(), true,ThreadLocalRandom.current().nextInt(0, 100000 + 1)));
        respuestas.add(new Respuesta(sc.next(), false,ThreadLocalRandom.current().nextInt(0, 100000 + 1)));
        respuestas.add(new Respuesta(sc.next(), false,ThreadLocalRandom.current().nextInt(0, 100000 + 1)));
        respuestas.add(new Respuesta(sc.next(), false,ThreadLocalRandom.current().nextInt(0, 100000 + 1)));
        array.add(sp);
//        out.println(sp.getPregunta());
        //out.println(sp.toString());
        //out.println("<br>");

    }
    //out.println("<br><br>");
    //out.println(array.size());
    //out.println("<br>");
    Collections.shuffle(array);
    for (Iterator<Pregunta> it = array.iterator(); it.hasNext();) {
        Pregunta elem = it.next();
        Collections.shuffle(elem.getRespuestas());
        //out.println(elem.toString());
        //out.println("<br>");
    }
    HttpSession sesionOk = request.getSession();
    sesionOk.setAttribute("Questions", array);
%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Certificacion Financiera</title>
        <link rel="stylesheet" href="css/style.css">
        <meta name="viewport" content="width=device=width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"  crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
        <link id="favicon" rel="icon" type="image/png" href="images/favicon.ico">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" crossorigin="anonymous">
        <link href="https://fonts.googleapis.com/css?family=Noto+Sans+KR" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
        <link rel="stylesheet" h     ref="css/animate.css">

    </head>
    <body>
        <%@include file="header.jsp"%> 
        
        <article>
            <form class="form-horizontal" action="checkCert_IntroFinQuiz.jsp" method="POST"">
                <fieldset>

                    <!-- Form Name -->
                    <legend>Certificacion</legend>
                    <!--Cronometro-->
                    <script>
                        var centesimas = 0;
                        var segundos = 0;
                        var minutos = 0;
                        var horas = 0;
                        function inicio () {
                                control = setInterval(cronometro,10);
                               
                        }
                        function cronometro () {
                            if (centesimas < 99) {
                                    centesimas++;
                                    if (centesimas < 10) { centesimas = "0"+centesimas }
                                    Centesimas.innerHTML = ":"+centesimas;
                            }
                            if (centesimas == 99) {
                                    centesimas = -1;
                            }
                            if (centesimas == 0) {
                                    segundos ++;
                                    if (segundos < 10) { segundos = "0"+segundos }
                                    Segundos.innerHTML = ":"+segundos;
                            }
                            if (segundos == 59) {
                                    segundos = -1;
                            }
                            if ( (centesimas == 0)&&(segundos == 0) ) {
                                    minutos++;
                                    if (minutos < 10) { minutos = "0"+minutos }
                                    Minutos.innerHTML = ":"+minutos;
                            }
                            if (minutos == 59) {
                                    minutos = -1;
                            }
                            if ( (centesimas == 0)&&(segundos == 0)&&(minutos == 0) ) {
                                    horas ++;
                                    if (horas < 10) { horas = "0"+horas }
                                    Horas.innerHTML = horas;
                            }
                    }
                    $(document).ready(function() {
                        $("#Enviar").on("click", function() {
                            $.ajax({
                                url: "checkCert_IntroFinQuiz.jsp",
                                method: "POST",
                                data: "Minutos" + $(".reloj").text(),
                                data: "Segundos" + $(".reloj").text(),
                                data: "Centesimas" + $(".reloj").text(),
                                success: function(response) {
                                    //handle response
                                }
                            })
                        })

                    })
                    inicio();
                    </script>
                    <div id="contenedor">
                        <h5 style="display:inline-block">Tiempo: </h5>
                    <div class="reloj" id="Minutos" style="display:inline-block"> 00</div>
                    <div class="reloj" id="Segundos" style="display:inline-block">:00</div>
                    <div class="reloj" id="Centesimas" style="display:inline-block">:00</div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="textinput">Nombre: </label>  
                        <div class="col-md-4">
                            <input id="nombre" name="nombre" type="text" placeholder="Ingresa tu nombre" class="form-control input-md" required>
                        </div>
                    </div>
                    <input id="fecha" name="fecha" type="hidden" value="<% 
                        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");  
                        LocalDateTime now = LocalDateTime.now();  
                        out.println(dtf.format(now));  %>" class="form-control input-md" required>
                    
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="textinput2">Ciudad: </label>  
                        <div class="col-md-4">
                            <input id="ciudad" name="ciudad" type="text" placeholder="Ingresa tu ciudad" class="form-control input-md" required>
                        </div>
                    </div>
                    <input id="company" name="company" type="hidden" value="AWS" class="form-control input-md" required>
                    <input id="company" name="cert" type="hidden" value="Introduccion a Finanzas" class="form-control input-md" required>
                    <input id="instructor" name="instructor" type="hidden" value="Kerrisk" class="form-control input-md" required>
                    <%
                        Iterator<Pregunta> it = array.iterator();
                        for (int i = 0; i < 8; i++) {
                            Pregunta elem = it.next();
                    %>
                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="textinput">Pregunta <%=i + 1%>: </label>  
                        <div class="col-md-4">
                            <input id="pregunta<%=i%>" name="pregunta<%=i%>" type="text" value="<%=elem.getPregunta()%>" class="form-control input-md" disabled>
                        </div>
                    </div>

                    <!-- Multiple Radios -->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="radios">Escoge una Respuesta</label>
                        <div class="col-md-4">
                            <div class="radio">
                                
                                    <%
                                        int j = 0;
                                        for (Iterator<Respuesta> ir = elem.getRespuestas().iterator(); ir.hasNext();) {

                                            Respuesta resp = ir.next();
                                    %>    
                                    <label for="respuesta<%=i%><%=j%>">
                                    <input type="radio" name="radios-<%=i%>" id="respuesta<%=i%><%=j%>" value="<%=resp.getValor()%>">
                                    <%=resp.getRespuesta()%><br>
                                    </label>
                                    

                                    <%
                                            j++;
                                        }
                                    %>
                                
                            </div>


                        </div>
                    </div>
                    <%
                        }
                    %>
                    <!-- Button -->
                    <div class="form-group">
                        <div class="col-md-4">
                            <input type="submit" value="Enviar" id="Enviar" class="btn btn-primary">
                        </div>
                    </div>
                </fieldset>
            </form>

        </article>

        <footer class="fakefooter">
            <div class="left">
                <span>2018 <i class="far fa-registered"></i>AWS Certificaciones. Todos los derechos reservados. 3-297-15728-0.</span>
            </div>
            <div class="right green">
                @AWS_latam
            </div>
        </footer>

    </body>
</html>