<%-- 
    Document   : VerPDF
    Created on : 29-oct-2018, 0:03:03
    Author     : Kristian
--%>
<%
    String User = "";
    HttpSession sesionOK = request.getSession();
    Boolean loggedin = sesionOK.getAttribute("User") != null;
    if (loggedin) {
        User = (String) sesionOK.getAttribute("User");
    }
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <title><%=User%></title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    
    <body>
        <svg height="2000" width=100%" >
            <defs>
              <filter id="f1" x="0" y="0" width="200%" height="200%">
                <feOffset result="offOut" in="SourceAlpha" dx="20" dy="20" />
                <feGaussianBlur result="blurOut" in="offOut" stdDeviation="10" />
                <feBlend in="SourceGraphic" in2="blurOut" mode="normal" />

              </filter>
            </defs>

            <rect width="90%" height="90%" stroke="white" stroke-width="3" fill="yellow" filter="url(#f1)" />
            <rect width="89%" height="89%" stroke="red" stroke-width="8" fill="white"  />

            <!--Diseño estrellas-->

            <!--<polygon points="900,10 40,198 190,78 10,78 160,198" style="fill:black;stroke:black;stroke-width:5;fill-rule:nonzero;"/>-->
            <image xlink:href="images/medalla.jpeg" width="15%" height="15%" x="40%" y="2%"/>
            <text x="20%" y="20%" fill="Black" style="font-family: Times New Roman;
                         font-size  : 54;
                         fill       : #4d4d4d;" >CERTIFICADO COMPLETADO</text>
            <text x="30%" y="25%" fill="Black" style="font-family: Times New Roman;
                  font-size  : 26; ;" >CERTIFICADO DE <%=(String) sesionOK.getAttribute("cert")%></text>
            <text x="30%" y="30%" fill="Black" style="font-family: Times New Roman;
                         font-size  : 26; ;" >A NOMBRE DE: <%=(String) sesionOK.getAttribute("nombre")%></text>
            <text x="30%" y="35%" fill="Black" style="font-family: Times New Roman;
                         font-size  : 26; ;" >CALIFICACION: <%= sesionOK.getAttribute("calif")%></text>
            <text x="30%" y="40%" fill="Black" style="font-family: Times New Roman;
                         font-size  : 26; ;" >FECHA DE EXPEDICIÓN: <%=(String) sesionOK.getAttribute("fecha")%></text>
            <text x="30%" y="45%" fill="Black" style="font-family: Times New Roman;
                         font-size  : 26; ;" >COMPAÑIA: <%=(String) sesionOK.getAttribute("company")%></text>
            <text x="30%" y="50%" fill="Black" style="font-family: Times New Roman;
                         font-size  : 26; ;" >INSTRUCTOR: <%=(String) sesionOK.getAttribute("instructor")%></text>
            


          </svg>
        <form name="Pdf" action="/ExamenPractico/pdf">
            <h1>Datos</h1>
            <input type="text" name="Text_name" value=""    placeholder="Ingresa nombre completo" size="80"/>
            <input type="submit" value="Ver PDF" name="Btn_ver" />
        </form>
    </body>
</html>
