<%-- 
    Document   : header
    Created on : 28/10/2018, 02:51:09 PM
    Author     : erikgoh
--%>
<%
    String User = "";
    HttpSession sesionOK = request.getSession();
    Boolean loggedin = sesionOK.getAttribute("User") != null;
    if (loggedin) {
        User = (String) sesionOK.getAttribute("User");
    }
%>
<script>
    function validarPassword(f){
        var p1 = document.getElementById("PasswordR");
        var p2 = document.getElementById("PasswordR2");
        if (f.PasswordR.value === f.PasswordR2.value){
            return true;
        }
        else {
            alert("Las contrase�as no coinciden");
            return false;
        }
    }
    </script>
<header>
    <div class="left">
        <a href="index.jsp"><img src="images/prod-art-aws-600.width-1200.png" alt=""></a>
    </div>
    <div class="center">
        <nav>
            <ul>
                <li><a href="aboutus.jsp">Nosotros</a></li>
                <li class="dropdown">
                    <a href="#" class="dropbtn">Certificaciones Developer<i class="fas fa-caret-down"></i></a>
                    <div class="dropdown-content">
                        <a href="Cert_java.jsp">Java</a>
                        <a href="Cert_web.jsp">Web Developer</a>
                    </div>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropbtn">Certificaciones Financieras<i class="fas fa-caret-down"></i></a>
                    <div class="dropdown-content">
                        <a href="Cert_FinPer.jsp">Finanzas Personales</a>
                        <a href="Cert_IntroFin.jsp">Introduccion a las finanzas</a>
                    </div>
                </li>
                <li><a href="#">Soporte</a></li>
                <li><a href="#">Atenci�n al cliente</a></li>
                <li><a href="#">Contacto</a></li>
            </ul>
        </nav>
    </div>
    <div class="right">
        <%
            if (loggedin) {%>
        <div class="top">
            <%=User%>
        </div>
        <div class="bottom">
            <a href="cerrarsesion.jsp">Log out</a>
        </div>
        <%} else { %>
        <!-- Button trigger modal -->
        <button type="button" class="btn btn-login btn-sm" data-toggle="modal" data-target="#login">
            Login
        </button>
        <!-- Modal -->
        <div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="loginTitle" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="loginTitle">Login</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="checklogin.jsp" method="POST">
                            <div class="form-group">
                                <label for="User">Usuario</label>
                                <input type="text" required class="form-control" id="User" aria-describedby="emailHelp" placeholder="Ingresa tu usuario" name="User">
                            </div>
                            <div class="form-group">
                                <label for="Password">Password</label>
                                <input type="password" required class="form-control" id="Password" placeholder="Password" name="Password">
                            </div>
                            <button type="submit" class="btn btn-primary">Enviar</button>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button data-dismiss="modal" type="button"  class="btn btn-login btn-sm" data-toggle="modal" data-target="#registro" >Registro</button>


                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>

        </div>
        <!-- Modal de Registro-->
        <div class="modal fade" id="registro" tabindex="-1" role="dialog" aria-labelledby="registroTitle" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="registroTitle">Registro</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="checkregistro.jsp" method="POST" onsubmit="return validarPassword(this);">
                            <div class="form-group">
                                <label for="User">Usuario</label>
                                <input type="text" required class="form-control" id="UserR" aria-describedby="emailHelp" placeholder="Ingresa tu usuario" name="UserR">
                            </div>
                            <div class="form-group">
                                <label for="Password">Password</label>
                                <input type="password" required class="form-control" id="PasswordR" placeholder="Password" name="PasswordR">
                            </div>
                            <div class="form-group">
                                <label for="Password">De nuevo Password</label>
                                <input type="password" required class="form-control" id="PasswordR2" placeholder="Reingresa tu Password" name="PasswordR2">
                            </div>
                            <button type="submit" class="btn btn-primary">Enviar</button>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <% }%>
    </div>
</header>