<%-- 
    Document   : Cert_java
    Created on : 29/10/2018, 01:10:30 AM
    Author     : erikgoh
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Certificacion Developer</title>
        <link rel="stylesheet" href="css/style.css">
        <meta name="viewport" content="width=device=width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"  crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
        <link id="favicon" rel="icon" type="image/png" href="images/favicon.ico">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" crossorigin="anonymous">
        <link href="https://fonts.googleapis.com/css?family=Noto+Sans+KR" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
        <link rel="stylesheet" href="css/animate.css">
    </head>
    <body>
        <%@include file="header.jsp"%>
        <article>
            <h3>Certificacion Java</h3>
            <br>
            <hr>
            <br>
            <p>¿Cuáles son los objetivos de este curso?</p>
            <p>
                Te enseñaremos paso a paso y desde cero cómo construir aplicaciones Java de verdad, considerando las técnicas de desarrollo utilizando programación orientada a objetos, utilizando los componentes más importantes de Java como conexiones a base de datos con JDBC, crear aplicaciones de ventana o escritorio con Swing y por su puesto aprenderemos a manejar los errores y excepciones de nuestro sistema, entregando material y contenido de calidad.
            </p>
            <br>
            <p>¿A quién va dirigido?</p>
            <p>Profesionales y estudiantes en el área de tecnología de información e informática con la intención de especializarse en el desarrollo de aplicaciones a través de un enfoque de Programación Orientada a Objetos, actualizado, aun nivel más cercano a la Ingeniería de Software implementando el desarrollo en 3 capas MVC.
            </p>
            <br>
            <p>Requisitos</p>
            <p>
                Conocimientos básicos de programación tales como entender la importancia de la reusabilidad y mantenibilidad de un código. Conceptos de variable e identificador. Sentencias de control de flujo. if, else, else if, switch, case, break, default, while, do, while, for.
            </p>
            <%
                 if (!loggedin) { %>
            <h4>Debes Registrarte para poder hacer el examen de certificacion.</h4>
            <button data-dismiss="modal" type="button"  class="btn btn-login btn-sm" data-toggle="modal" data-target="#registro" >Registro</button>  
            <%
            } else { %>
            <h4>El examen de certificacion no esta disponible en estos momentos.</h4> 
            <button type="button"  class="btn btn-primary btn-sm disabled"><a href="">Iniciar Test</a></button>  
            <%
                }
            %>
        </article>
        <jsp:include page="footer.html"/>
    </body>
</html>
