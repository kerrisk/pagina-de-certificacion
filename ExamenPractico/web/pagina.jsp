<%-- 
    Document   : pagina
    Created on : 28/10/2018, 02:53:34 PM
    Author     : erikgoh
--%>

<!DOCTYPE html>
<html lang="es">
<head>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"  crossorigin="anonymous">
	<meta charset="UTF-8">
	<title>Viajes Venezuela</title>
	<link rel="stylesheet" href="css/style.css">
	<meta name="viewport" content="width=device=width, initial-scale=1, shrink-to-fit=no">
	<link id="favicon" rel="icon" type="image/png" href="images/favicon.png">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css"  crossorigin="anonymous">
	<link href="https://fonts.googleapis.com/css?family=Noto+Sans+KR" rel="stylesheet">

</head>
<body>
	<jsp:include page="header.jsp"/>
	<nav>
		<ul>
			<li><a href="index.html">Nosotros</a></li>
			<li class="dropdown">
				<a href="pagina.html" class="dropbtn">Servicios <i class="fas fa-caret-down"></i></a>
				<div class="dropdown-content">
					<a href="pagina.html">Viajes</a>
					<a href="pagina.html">Reservaciones</a>
					<a href="pagina.html">Tours</a>
				</div>
			</li>
			<li class="dropdown">
				<a href="#" class="dropbtn">Paquetes <i class="fas fa-caret-down"></i></a>
				<div class="dropdown-content">
					<a href="#">Individual</a>
					<a href="#">Familiar</a>
					<a href="#">Grupal</a>
				</div>
			</li>
			<li><a href="#">Promociones</a></li>
			<li><a href="pagina2.html">Atenci�n al cliente</a></li>
			<li><a href="pagina3.html">Contacto</a></li>
		</ul>
	</nav>
	<article>
		<form class="form-horizontal">
		<fieldset>
			<!-- Form Name -->
			<legend>Reserve su vuelo</legend>

			<!-- Text input-->
			<div class="form-group">
			  <label class="col-md-4 control-label" for="textinput">Origen</label>  
			  <div class="col-md-4">
			  <input id="textinput" name="textinput" type="text" placeholder="Aguascalientes, Aguascalientes" class="form-control input-md" required="">
			  <span class="help-block">De donde quieres partir</span>  
			  </div>
			</div>

			<!-- Text input-->
			<div class="form-group">
			  <label class="col-md-4 control-label" for="textinput2">Destino</label>  
			  <div class="col-md-4">
			  <input id="textinput2" name="textinput2" type="text" placeholder="CDMX" class="form-control input-md" required="">
			  <span class="help-block">A donde quieres llegar.</span>  
			  </div>
			</div>
			<!-- Date -->
			<div class="form-group">
			  <label class="col-md-4 control-label" for="date">Fecha</label>  
			  <div class="col-md-4">
			  <input id="date" name="fecha" type="date" class="form-control input-md" required="">
			  <span class="help-block">Fecha de salida</span>  
			  </div>
			</div>
			<!-- Multiple Radios (inline) -->
			<div class="form-group">
			  <label class="col-md-4 control-label" for="radios">Personas</label>
			  <div class="col-md-4"> 
			    <label class="radio-inline" for="radios-0">
			      <input type="radio" name="radios" id="radios-0" value="1" checked="checked">
			      1
			    </label> 
			    <label class="radio-inline" for="radios-1">
			      <input type="radio" name="radios" id="radios-1" value="2">
			      2
			    </label> 
			    <label class="radio-inline" for="radios-2">
			      <input type="radio" name="radios" id="radios-2" value="3">
			      3
			    </label> 
			    <label class="radio-inline" for="radios-3">
			      <input type="radio" name="radios" id="radios-3" value="4">
			      4
			    </label>
			  </div>
			</div>

			<!-- Select Basic -->
			<div class="form-group">
			  <label class="col-md-4 control-label" for="selectbasic">Categor�a de Vuelo</label>
			  <div class="col-md-4">
			    <select id="selectbasic" name="selectbasic" class="form-control">
			      <option value="1">Economica</option>
			      <option value="2">Ejecutiva</option>
			      <option value="3">Primera Clase</option>
			    </select>
			  </div>
			</div>

			<!-- Button -->
			<div class="form-group">
			  <label class="col-md-4 control-label" for="submit"></label>
			  <div class="col-md-4">
			    <button id="submit" name="submit" class="btn btn-primary">Submit</button>
			  </div>
			</div>
			
		</fieldset>
		</form>
	</article>
	<footer>
		<div class="left">
			<span>2018 <i class="far fa-registered"></i>Viajes Uniglobe. Todos los derechos reservados. 3-297-15728-0.</span>
		</div>
		<div class="right green">
			@uniglobe_ve
		</div>
	</footer>
</body>
</html>
