<%-- 
    Document   : Cert_IntroFin
    Created on : 29/10/2018, 01:10:21 AM
    Author     : erikgoh
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Certificacion Financiera</title>
        <link rel="stylesheet" href="css/style.css">
        <meta name="viewport" content="width=device=width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"  crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
        <link id="favicon" rel="icon" type="image/png" href="images/favicon.ico">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" crossorigin="anonymous">
        <link href="https://fonts.googleapis.com/css?family=Noto+Sans+KR" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
        <link rel="stylesheet" href="css/animate.css">
    </head>
    <body>

        <%@include file="header.jsp"%>
        <article>
            <h3>Certificacion Introduccion a las Finanzas</h3>
            <br>
            <hr>
            <br>
            <h1>Acerca de este curso</h1>
            <p>En este certificacion se evaluará el objetivo principal de las finanzas, la importancia en la creación de valor y mantenerlo a través de un uso eficiente de los recursos financieros.
            </p>
            <p>Además, incluiremos algunas herramientas básicas que te permitirán crear un plan financiero con el que podrás establecer objetivos y metas claras, identificar qué factores pueden afectar el rumbo de tu empresa o idea de negocio y saber si estás aplicando los recursos de una manera eficiente </p>
            <br>
            <h1>Objetivos de Certificación</h1>
            <p>El solicitante debe saber aplicar herramientas básicas de análisis, control y evaluación de información financiera para una adecuada gestión de negocio. </p>
            <%
                if (!loggedin) { %>
            <h4>Debes Registrarte para poder hacer el examen de certificacion.</h4>
            <button data-dismiss="modal" type="button"  class="btn btn-login btn-sm" data-toggle="modal" data-target="#registro" >Registro</button>  
            <%
        } else { %>
        <button type="button"  class="btn btn-primary btn-sm"><a href="Cert_IntroFinQuiz.jsp">Iniciar Test</a></button>  
            <%
                }
            %>
        </article>

        <jsp:include page="footer.html"/>
    </body>
</html>