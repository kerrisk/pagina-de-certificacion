<%-- 
    Document   : Cert_web
    Created on : 29/10/2018, 01:10:40 AM
    Author     : erikgoh
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Certificacion Developer</title>
        <link rel="stylesheet" href="css/style.css">
        <meta name="viewport" content="width=device=width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"  crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
        <link id="favicon" rel="icon" type="image/png" href="images/favicon.ico">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" crossorigin="anonymous">
        <link href="https://fonts.googleapis.com/css?family=Noto+Sans+KR" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
        <link rel="stylesheet" href="css/animate.css">
    </head>
    <body>
        <%@include file="header.jsp"%>
        <article>
            <h3>Certificacion Desarrolo de Aplicaciones IOS</h3>
            <br>
            <hr>
            <br>
            <h1>Descripción</h1>
            <p>¿Te gustaría saber cómo desarrollar una aplicación? El curso de Experto en Desarrollo de Aplicaciones que ofrece nuestra plataforma a través de AWS Certification está dirigido a todas aquellas personas que tengan conocimientos de programación y estén deseos de mejorar sus aptitudes y ser capaces de crear su propia aplicación
            </p>
            <h1>Duracion</h1>
            <p>El curso se imparte de forma online y tiene una duración total de 60 horas de estudio. AWS te ofrece una flexibilidad horaria que te permitirá estudiar en tus ratos libre y compaginar tu formación con otras actividades. Al finalizar la formación recibirás un certificado acreditativo otorgado por el centro. </p>

            <h1>Objetivos de Certificacion</h1>
            <p>En este certificacion nos aseguraremosnque estes familiarizado con el entorno de desarrollo de iOS, creación de proyectos de Xcode,uso de Interface Builder, etc.Que estas capacitado para realizar la creación del proyecto, la creación de un interfaz sencillo y sabes cómo conectar el interface y el código. </p>
            <%
                 if (!loggedin) { %>
            <h4>Debes Registrarte para poder hacer el examen de certificacion.</h4>
            <button data-dismiss="modal" type="button"  class="btn btn-login btn-sm" data-toggle="modal" data-target="#registro" >Registro</button>  
            <%
            } else { %>
            <h4>El examen de certificacion no esta disponible en estos momentos.</h4> 
            <button type="button"  class="btn btn-primary btn-sm disabled"><a href="">Iniciar Test</a></button>  
            <%
                }
            %>
        </article>
        <jsp:include page="footer.html"/>
    </body>
</html>