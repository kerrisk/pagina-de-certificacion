<%-- 
    Document   : checkCert_IntroFinQuiz
    Created on : 29/10/2018, 04:18:11 AM
    Author     : erikgoh
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.Collections"%>
<%@page import="java.util.Collection"%>
<%@page import="java.util.ArrayList"%>
<%@page session="true" %>
<%@page import="misclases.Pregunta" %>
<%@page import="misclases.Respuesta" %>
<%@ page import = "javax.servlet.RequestDispatcher" %>
<%@page import="java.nio.file.Paths"%>
<%@page import="java.nio.file.StandardOpenOption"%>
<%@page import="java.nio.file.OpenOption"%>
<%@page import="java.nio.file.Files"%>
<%@page import="java.io.PrintWriter"%>
<%@page import="java.io.FileWriter"%>
<%@page import="java.io.BufferedWriter"%>
<%@page import="java.io.File" %>
<%@page import="java.io.FileNotFoundException" %>
<%@page import="java.util.Scanner" %>
<%
    ArrayList<Pregunta> array = new ArrayList();
    String UserL = "";
    HttpSession sesionOKL = request.getSession();
    Boolean loggedinL = sesionOKL.getAttribute("User") != null;
    if (loggedinL) {
        UserL = (String) sesionOKL.getAttribute("User");
        array = (ArrayList<Pregunta>) sesionOKL.getAttribute("Questions");
        sesionOKL.setAttribute("nombre", request.getParameter("nombre"));
        sesionOKL.setAttribute("fecha", request.getParameter("fecha"));
        sesionOKL.setAttribute("ciudad", request.getParameter("ciudad"));
        sesionOKL.setAttribute("company", request.getParameter("company"));
        sesionOKL.setAttribute("instructor", request.getParameter("instructor"));
        sesionOKL.setAttribute("cert", request.getParameter("cert"));
        
    }
    int Calif = 0;

%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Resultado</title>
        <link rel="stylesheet" href="css/style.css">
        <meta name="viewport" content="width=device=width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"  crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
        <link id="favicon" rel="icon" type="image/png" href="images/favicon.ico">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" crossorigin="anonymous">
        <link href="https://fonts.googleapis.com/css?family=Noto+Sans+KR" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
        <link rel="stylesheet" href="css/animate.css">
    </head>
    <body>
        <%            Iterator<Pregunta> it = array.iterator();
            for (int i = 0; i < 12 && it.hasNext(); i++) {
                Pregunta elem = it.next();
                int j = 0;
                for (Iterator<Respuesta> re = elem.getRespuestas().iterator(); re.hasNext();) {
                    Respuesta elemRE = re.next();
                    out.println(elemRE.getValor());
                    if (request.getParameter("radios-" + i) != null) {
                        if (elemRE.getValor() == Integer.parseInt(request.getParameter("radios-" + i))) {
                            out.println(elemRE.getValor());
                            out.println(" ; ");
                            out.println(request.getParameter("respuesta" + i));
                            out.println(" <br> ");
                            if (elemRE.getCorrecta()) {
                                Calif++;
                            }
                        }
                    }

                    j++;
                }

            }
            String Archivo = "C:/pruebas/miPersonal.txt";
            sesionOKL.setAttribute("nombre", request.getParameter("nombre"));
        sesionOKL.setAttribute("fecha", request.getParameter("fecha"));
        sesionOKL.setAttribute("ciudad", request.getParameter("ciudad"));
        sesionOKL.setAttribute("company", request.getParameter("company"));
        sesionOKL.setAttribute("instructor", request.getParameter("instructor"));
        sesionOKL.setAttribute("cert", request.getParameter("cert"));
            String Registro=request.getParameter("nombre")+" "+request.getParameter("cert")+" "+Calif+" ";
            if (Calif<5)
                Registro += "Reprobado";
            else
                Registro += "Aprobado ";
            Registro+=request.getParameter("fecha");
            Files.write(Paths.get(Archivo), Registro.getBytes(),StandardOpenOption.CREATE,StandardOpenOption.APPEND);
            sesionOKL.setAttribute("calif", Calif);
        %>


        <%
//        RequestDispatcher rd = request.getRequestDispatcher("/ExamenPractico/pdf");
//        request.setAttribute("Text_name",request.getParameter("nombre"));
//        rd.forward(request, response);
        %>
        <%@include file="header.jsp"%>
        <%
            if (loggedin) {

            } else {
        %>
        <jsp:forward page="index.jsp" />
        <%
            }
            if (Calif < 2) {%>

        <h1>No aprobaste :c</h1>
        <h2>Aciertos=<%= Calif%></h2>

        <%} else {
        %>

        <jsp:forward page="VerPDF.jsp"/>
        <% }%>
        <jsp:include page="footer.html"/>
    </body>
</html>
