<%-- 
    Document   : aboutus
    Created on : 29/10/2018, 12:25:55 AM
    Author     : erikgoh
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
         <meta charset="UTF-8">
        <title>Acerca de Nosotros</title>
        <link rel="stylesheet" href="css/style.css">
        <meta name="viewport" content="width=device=width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"  crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
        <link id="favicon" rel="icon" type="image/png" href="images/favicon.ico">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" crossorigin="anonymous">
        <link href="https://fonts.googleapis.com/css?family=Noto+Sans+KR" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
        <link rel="stylesheet" href="css/animate.css">
    </head>
    <body>
        <jsp:include page="header.jsp"/>
        <article></article>
     
        <article>
           <div>
            <img src="images/prod-art-aws-600.width-1200.png" alt="">
        </div> 
            <hr>
            <h3 style="text-align: center">AWS ó NADIE.</h3>
            <h6 style="text-align: center">Somos la unica compañia web de examenes en linea certificada con el estandar IP 68.32</h6>
            <br>
            <h6 style="text-align: center">nuestros examenes e instructores han recibido a lo largo del nuestros 10 años en el </h6>
            <br>
            <h6 style="text-align: center">mercado distintos reconocimientos como:</h6>
            <br>
            <ul style="text-align: center">
                <li style="text-align: center">Mejor Plataforma de cursos habla hispana Online 2016</li>
                <br>
                <li style="text-align: center">Top 10 Web Certification Awards 2017</li>
                <br>
                <li style="text-align: center">Google Awards 2014 mejor pagina web para emprendedores</li>
                <br>
                <li style="text-align: center">Google Awards 2014 mejor pagina web para desarroladores </li>
                
        </article>
        <footer class="fakefooter">
            <div class="left">
                <span>2018 <i class="far fa-registered"></i>AWS Certificaciones. Todos los derechos reservados. 3-297-15728-0.</span>
            </div>
            <div class="right green">
                @AWS_latam
            </div>
        </footer>
        
    </body>
</html>
